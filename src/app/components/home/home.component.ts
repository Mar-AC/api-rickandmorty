import { Component } from '@angular/core';
import { RymService } from 'src/app/services/RickAndMorty/rym.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  public personajes: any[]=[];
  constructor(private _rym: RymService){ }

  ngOnInit(): void {
    this.getPersonajes();
  }
  getPersonajes(){
    this._rym.getPersonajesRM()
    .subscribe((data:any)=>{
      console.log(data);
      this.personajes=data.results;
      console.log('personajes seteados', this.personajes);
    });
  }

}
