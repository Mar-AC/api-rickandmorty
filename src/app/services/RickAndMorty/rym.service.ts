import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RymService {
  public URL='https://rickandmortyapi.com/api';

  constructor( private http: HttpClient) { }
  getPersonajesRM(){
    const uri = `${this.URL}/character`;
    return this.http.get(uri);
  }
}
